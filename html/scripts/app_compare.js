let selected = {};

document.querySelectorAll('.select_button').forEach((element) => {
    element.addEventListener('click', function(e) {
        if(selected[e.target.id] != undefined) {
            delete selected[e.target.id];
            e.target.src = '/media/select_line.svg';
            render_metadata();
            if(Object.values(selected).length == 0) {
                hidecompare();
            }
        } else {
            e.target.src = '/media/select_full.svg';
            get_metadata(e.target.id);
            showcompare();
        }
    })
});

function get_metadata(id) {
    const url = `/ajax?action=get_metadata&id=${id}`;
    console.log(url);
    fetch(url).then((response) => {return response.json()})
    .then((data) => {
        if(data.length > 0) {
            selected[id] = data[0];
            render_metadata();
        } 
    });
}

function render_metadata() {
    let html = '';
    if(Object.values(selected).length > 0) {
        Object.values(selected).forEach((object) => {
            console.log(object);
            html += `
                <ul>
                    <strong>${object.title}</strong>
                    <li><span>Prijs: </span><strong>${object.specs.price}</strong></li>
                    <li><span>Advertenties: </span><strong>${object.specs.ads}</strong></li>
                    <li><span>Platform: </span<strong>${object.specs.platform}</strong></li>
                    <li><span>Bestandstypen: </span<strong>${object.specs.filetypes}</strong></li>
                </ul>`
        });
        document.querySelector('.compare_wrap').innerHTML = html;
    } else {
        document.querySelector('.compare_wrap').innerHTML = '';   
    }
}

let compare_shown = false;

function showcompare() {
    if(compare_shown == false) {
        anime({
            targets: '.compare_prompt',
            keyframes: [
                {translateX:'-600px', duration: 1},
                {translateX: '0', duration: 300},
            ],
            easing: "easeInOutCubic",
            begin: () => {
                document.querySelector('.compare_prompt').style.display = 'block';
            }
        })
        compare_shown = true;
    }
}
function hidecompare() {
    if(compare_shown == true) {
        anime({
            targets: '.compare_prompt',
            keyframes: [
                {translateX:'-600px', duration: 300},
            ],
            easing: "easeInOutCubic",
            complete: () => {
                document.querySelector('.compare_prompt').style.display = 'none';
            }
        })
        compare_shown = false;
    }
}