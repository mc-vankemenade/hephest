let listitems = document.querySelectorAll('li');

listitems.forEach((item) => {
    item.style.marginTop = '500px';
})

anime({
    targets: 'li',
    marginTop: 0,
    delay:anime.stagger(200),
    duration: 1000,
    easing: 'easeOutQuint',
})

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

document.querySelectorAll('.like_button').forEach((button) => {
    button.addEventListener("click", () => {
        if (getCookie("logged_in") != "") {
            const re = new RegExp("heart_full.svg");
            console.log(button.src);
            if (re.test(button.src)) {
                button.src = '/media/heart_line.svg';
            } else {
                button.src = '/media/heart_full.svg';
            }
            
        } else {
            toggle_login();
        }
    })
})

document.querySelectorAll('.price_wrap').forEach((element) => {
    switch(element.innerHTML){
        case 'Gratis': {
            element.style.color = '#a6d189';
            break;
        }
        case 'Freemium': {
            element.style.color = '#f9cb6b';
            break;
        }

        default: {
            element.style.color = '#ef9f76';
        }
    }
})