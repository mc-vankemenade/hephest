function animate_up() {
    const search_bar = document.querySelector('.search_bar');
    const background = document.querySelector('.background-big');

    
}

// Animation for the search submit button
document.querySelector('.submit_button').addEventListener('click', () => {

    const form = document.querySelector('.search_form');

    form.style.display = 'none';
    form.style.justifyContent = 'right';
    document.querySelector('.submit_button').style.display = 'none';
    anime({
        targets: '.search_bar',
        translateY: -275,
        width: '80%',
        height: '25px',
        easing: 'easeInOutSine',
        duration: 300,
    })

    anime({
        targets: '.background-big',
        translateY: -800,
        easing: 'easeInOutSine',
        duration: 500,
        complete: () => {
            form.submit()
          }
      });  
});

// Animation for the button to the explore page
document.querySelector('.discover_button').addEventListener('click', () => {
    const form =  document.querySelector('form')

    form.style.display = 'none';
    form.style.justifyContent = 'right';
    document.querySelector('.submit_button').style.display = 'none';
    anime({
        targets: '.search_bar',
        translateY: -275,
        width: '80%',
        height: '25px',
        easing: 'easeInOutSine',
        duration: 300,
    })

    anime({
        targets: '.background-big',
        translateY: -800,
        easing: 'easeInOutSine',
        duration: 500,
        complete: () => {
            window.location = '/discover'
          }
    });   
});

/* If browser back button was used, flush cache */
(function () {
	window.onpageshow = function(event) {
		if (event.persisted) {
			window.location.reload();
		}
	};
})();

