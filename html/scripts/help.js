// function hover(element){
//     element.addEventListener('mouseenter', () => {
//         window.addEventListener("wheel", scroll_horizontal, false);
//     });
//     element.addEventListener('mouseleave', () => {
//         window.removeEventListener("wheel", scroll_horizontal);
//     });
// }

// function scroll_horizontal(e) {
//     if (e.deltaY > 0) e.currentTarget.scrollLeft += 100;
//     else e.currentTarget.scrollLeft -= 100;
// }

// var help_lists = document.querySelectorAll(".help_wrap");

// help_lists.forEach((list) => {
//     hover(list)
// })

const helpWrap = document.querySelectorAll('.feed_wrap');
helpWrap.forEach((element) => {
    element.addEventListener('wheel', function(event) {
        element.scrollLeft += event.deltaY;
        event.preventDefault();
      });
})

const controlWrap = document.querySelectorAll('.control_wrap');
controlWrap.forEach((element) => {
    element.querySelector('.navigate.previous').addEventListener('click', function(event) {
        element.querySelector('.feed_wrap').scrollLeft -= 300;
    });
    element.querySelector('.navigate.next').addEventListener('click', function(event) {
        element.querySelector('.feed_wrap').scrollLeft += 300;
    })
});

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}


document.querySelectorAll('.like_button').forEach((button) => {
    button.addEventListener("click", () => {
        if (getCookie("logged_in") != "") {
            const re = new RegExp("heart_full.svg");
            console.log(button.src);
            if (re.test(button.src)) {
                button.src = '/media/heart_line.svg';
            } else {
                button.src = '/media/heart_full.svg';
            }
            
        } else {
            toggle_login();
        }
    })
})