const media_frame = document.querySelector('.media');
const next_media_frame = document.querySelector('.next_media');
const app_button = document.querySelector('.app_button');

const urlParams = new URLSearchParams(window.location.search);
let app_id = '*';
if(urlParams.get('app_id') != null) {
    app_id = urlParams.get('app_id');
} 

const cache_amount = 5;

let cached_posts = [];
let post_index = 0;
let total_posts = 0;

// get posts from backend
function getposts(amount, offset, app_id) {
    const url = `/ajax?action=get_posts&amount=${amount}&offset=${offset}&app_id=${app_id}`;
    console.log(url);
    fetch(url).then((response) => {return response.json()})
    .then((data) => {
        if(data.length > 0) {
            cached_posts = data; 
            post_index = 0;
            console.log(cached_posts);
            renderpost();
        } else {
            console.log("no posts found.starting from zero")
            total_posts = 0;
            getposts(3, 0, app_id);
        }
    });
}

function nextpost() {
    if(post_index == (cached_posts.length -1)) {
        console.log("At the end of posts. getting more");
        total_posts = total_posts + cache_amount;

        media_frame.style.transform = 'translate(0, 1000px)';

        getposts(cache_amount, total_posts, app_id);
        
        anime({
            targets: '.media',
            translateY: '-1000px',
            duration: 700,
            easing: 'easeOutSine',
        });
        anime({
            targets: '.next_media',
            translateY: '-1000px',
            duration: 700,
            easing: 'easeOutSine',
            complete : () => {
                next_media_frame.style.transform = 'translate(0, 0)';
            }
        });

    } else {
        post_index++;
        media_frame.style.transform = 'translate(0, 1000px)';
        renderpost();
        anime({
            targets: '.media',
            translateY: '-1000px',
            duration: 700,
            easing: 'easeOutSine',
        });
        anime({
            targets: '.next_media',
            translateY: '-1000px',
            duration: 700,
            easing: 'easeOutSine',
            complete : () => {
                next_media_frame.style.transform = 'translate(0, 0)';
            }
        });

    }
}

function prevpost() {
    if(post_index != 0) {
        media_frame.style.transform = 'translate(0, -1000px)';
        post_index--;
        renderpost();
        anime({
            targets: '.media',
            translateY: '1000px',
            duration: 700,
            easing: 'easeOutSine',
        });
        anime({
            targets: '.next_media',
            translateY: '1000px',
            duration: 700,
            easing: 'easeOutSine',
            complete : () => {
                next_media_frame.style.transform = 'translate(0, 0)';
            }
        });
    } 
}

function renderpost() {
    console.log(post_index)
    let post = cached_posts[post_index];
    if(post.is_embed == true) {
        media_frame.innerHTML = 
        `<iframe class="embedded_video" id="${post.id}" src=${post.url}?autoplay=1&controls=0&loop=1&disablekb=1$fs=0&modestbranding=1&enablejsapi=1&mute=1" frameborder="0" allow="autoplay; encrypted-media">
        </iframe><div class="video_overlay"></div>`;
    } else {
        media_frame.innerHTML = 
        `<img class="embedded_video" id="${post.id}" src=${post.url} />`;
    }

    app_button.href = `/results?app_id=${post.app_id}`;
    get_comments(post.id);
    get_apps(post.app_id);
    return
}

getposts(cache_amount, 0, app_id);

window.addEventListener('keydown', function(event) {
    const key = event.key; // "ArrowRight", "ArrowLeft", "ArrowUp", or "ArrowDown"

    switch (key) {
        case "ArrowUp":
            prevpost();
            break;
        case "ArrowDown":
            nextpost();
            break;
    }
});

document.querySelector('.bttn_previous').addEventListener('click', () => {
    prevpost();
});
document.querySelector('.bttn_next').addEventListener('click', () => {
    nextpost();
});

function fold_out(content) {
        let sidebar = document.querySelector('.sidebar');
        sidebar.style.display = 'flex';
        if (window.screen.width <= 600) {
            anime({
                targets: '.sidebar',
                keyframes: [
                    {height: '0%'},
                    {height: '35%'}
                ],
                duration: 300,
                easing: 'easeInOutQuint',
                begin: () => {
                    
                },
                complete: (anim) => {
                    content.style.display = 'flex';
                }
            });
        } else {
            anime({
                targets: '.sidebar',
                keyframes: [
                    {width: '0px'},
                    {width: '400px'}
                ],
                duration: 300,
                easing: 'easeInOutQuint',
                begin: () => {
                    
                },
                complete: (anim) => {
                    content.style.display = 'flex';
                }
            });
        }
        
}

function fold_in(content) {

    if (window.screen.width <= 600) {
        anime({
            targets: '.sidebar',
            keyframes: [
                {height: '35%'},
                {height: '0%'}
            ],
            duration: 300,
            easing: 'easeInOutQuint',
            begin: () => {
                content.style.display = 'none';
            },
            complete: (anim) => {
                let sidebar = document.querySelector('.sidebar');
                sidebar.style.display = 'none';
            }
        });
        
    } else {
        anime({
            targets: '.sidebar',
            keyframes: [
                {width: '400px'},
                {width: '0px'}
            ],
            duration: 300,
            easing: 'easeInOutQuint',
            begin: () => {
                content.style.display = 'none';
            },
            complete: (anim) => {
                let sidebar = document.querySelector('.sidebar');
                sidebar.style.display = 'none';
            }
        });
    }
}

document.querySelector('.bttn_comment').addEventListener('click', () => {
    if(document.querySelector('.wrap_applications').style.display == 'flex') {
        document.querySelector('.wrap_applications').style.display = 'none';

    }
    if (document.querySelector('.wrap_comments').style.display == 'flex') {
        fold_in(document.querySelector('.wrap_comments'));
    } else {
        fold_out(document.querySelector('.wrap_comments'));
    }
    
});

document.querySelector('.bttn_software').addEventListener('click', () => {
    if(document.querySelector('.wrap_comments').style.display == 'flex') {
        document.querySelector('.wrap_comments').style.display = 'none';

    }
    if (document.querySelector('.wrap_applications').style.display == 'flex') {
        fold_in(document.querySelector('.wrap_applications'));
    } else {
        fold_out(document.querySelector('.wrap_applications'));
    }
});

document.querySelector('.media').addEventListener('click', () => {
    if (document.querySelector('.wrap_applications').style.display == 'flex') {
        fold_in(document.querySelector('.wrap_applications'));
    } else if(document.querySelector('.wrap_comments').style.display == 'flex') {
        fold_in(document.querySelector('.wrap_comments'));
    }
});

document.querySelectorAll('.close_section').forEach((button) => {
    button.addEventListener('click', () => {
        if (document.querySelector('.wrap_applications').style.display == 'flex') {
            fold_in(document.querySelector('.wrap_applications'));
        } else if(document.querySelector('.wrap_comments').style.display == 'flex') {
            fold_in(document.querySelector('.wrap_comments'));
        }
    });
});


function get_comments(app_id) {
    const url = `/ajax?action=get_comments&app_id=${app_id}`;
    console.log(url);
    fetch(url).then((response) => {return response.json()})
    .then((data) => {
        let comment_wrap = document.querySelector('.comment_list');
        if(data.length > 0) {
            comment_wrap.innerHTML = '';
            data.forEach(element => {

                comment_wrap.innerHTML += `
                <li id=${element.id}>
                    <span class='username'>${element.username}: </span>${element.message}
                </li>
                `;
            });
        } else {
            console.log("no comments found")
            comment_wrap.innerHTML = "Nog geen comments 🙁"
        }
    });
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

function get_apps(app_id) {
    const url = `/ajax?action=get_apps&app_id=${app_id}`;
    console.log(url);
    fetch(url).then((response) => {return response.json()})
    .then((data) => {
        let comment_wrap = document.querySelector('.app_list');
        if(data.length > 0) {
            comment_wrap.innerHTML = '';
            data.forEach(element => {
                comment_wrap.innerHTML += `
                <li id=${element.id}>
                    <span class='app_name'>${element.title}</span>
                </li>
                `;
            });
        } else {
            console.log("No tools found")
            comment_wrap.innerHTML = "Er is geen software opgegeven 🙁"
        }
    });
}

function post_comment() {
    const comment = document.querySelector('.comment_body');
    const content = document.querySelector('.embedded_video');

    if(getCookie('logged_in') == "") {
        toggle_login();
        return;
    }

    if (comment.value.length == 0) {
        return;
    }

    const url = `/ajax?action=post_comment&comment=${comment.value}&post_id=${content.id}`;
    console.log(url);
    fetch(url).then((response) => {
        if(response.status == 200) {
            renderpost();
            comment.value = '';
        } else {
            console.log("Something went wrong");
        }
    });
}

var container = document.querySelector(".media");

container.addEventListener("touchstart", startTouch, false);
container.addEventListener("touchmove", moveTouch, false);

// Swipe Up / Down / Left / Right
var initialX = null;
var initialY = null;

function startTouch(e) {
  initialX = e.touches[0].clientX;
  initialY = e.touches[0].clientY;
};

function moveTouch(e) {
  if (initialX === null) {
    return;
  }

  if (initialY === null) {
    return;
  }

  var currentX = e.touches[0].clientX;
  var currentY = e.touches[0].clientY;

  var diffX = initialX - currentX;
  var diffY = initialY - currentY;

  if (Math.abs(diffX) < Math.abs(diffY)) {
    // sliding vertically
    if (diffY > 0) {
      // swiped up
      nextpost();
    } else {
      // swiped down
      prevpost();
    }  
  }

  initialX = null;
  initialY = null;

  e.preventDefault();
};

document.body.style.overflow = "hidden";

document.querySelectorAll('.bttn_like').forEach((button) => {
    button.addEventListener("click", () => {
        if (getCookie("logged_in") != "") {
            const re = new RegExp("heart_full.svg");
            console.log(button.src);
            if (re.test(button.children[0].src)) {
                button.children[0].src = '/media/heart_line.svg';
            } else {
                button.children[0].src = '/media/heart_full.svg';
            }
            
        } else {
            toggle_login();
        }
    })
})


