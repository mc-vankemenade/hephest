

function showNotifications() {
    let notes = document.querySelectorAll('.note');

    anime({
        targets: '.note',
        keyframes: [
            {translateY: '-500px', duration: 0},
            {translateY:'0', duration: 500},
            {delay: 10000},
            {translateX: '-1000px', duration: 200},
        ],
        delay: anime.stagger(500),
        easing: "easeInOutCubic",
        complete: () => {
            notes.forEach((note) => {
                note.style.display = 'none';
            })
        }
    })
}

showNotifications();

