let login_shown = false;
// Show/hide the login/registration prompt
document.querySelector('.button.profile').addEventListener('click', () => {toggle_login()});

function toggle_login() {
    const prompt = document.querySelector('.login_prompt');
    if(!login_shown) {
        prompt.style.display = "flex";
        anime({
            targets: '.login_prompt',
            right: '10px',
            easing: 'easeInOutSine',
            duration: 300,
        });
        login_shown = true;
    } else {
        anime({
            targets: '.login_prompt',
            right: '-400px',
            easing: 'easeInOutSine',
            duration: 300,
            complete: () => {
                prompt.style.display = 'none';
              }
        });
        login_shown = false;
    }
}

document.querySelector('.navigation .button').addEventListener('click', () => {
    const prompt = document.querySelector('.login_prompt');
    anime({
        targets: '.login_prompt',
        right: '-400px',
        easing: 'easeInOutSine',
        duration: 300,
        complete: () => {
            prompt.style.display = 'none';
          }
    });
    login_shown = false;
})



let register_state = false
let register_button = document.querySelector('.register_button');
if(register_button) {
    register_button.addEventListener('click', () => {
        if (register_state ==  false) {
            document.querySelector('.prompt_title').innerHTML = "Aanmelden";
            document.querySelector('input[name=action').value = "register";
            document.querySelector('button[type=submit').innerHTML = "Aanmelden";
            document.querySelector('.register_button').innerHTML = "Inloggen";
            document.querySelector('.prompt_text').innerHTML = "Al een account?";
            document.querySelector('input[name=username').type = "text";
            document.querySelector('label[for=username').style.display = "inline";
            register_state = true;
        } else {
            document.querySelector('.prompt_title').innerHTML = "Inloggen";
            document.querySelector('input[name=action').value = "login";
            document.querySelector('button[type=submit').innerHTML = "Inloggen";
            document.querySelector('.register_button').innerHTML = "Aanmelden";
            document.querySelector('.prompt_text').innerHTML = "Nog geen account?";
            document.querySelector('input[name=username').type = "hidden";
            document.querySelector('label[for=username').style.display = "none";
            register_state = false;
        }
        
    })
}
