<?php 
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../lib/db_helper.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__."/../..");
$dotenv->load();

require __DIR__ . '/session.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  switch($_POST["action"]) {
      case 'login': {
        if (isset($_POST["email"]) && isset($_POST["password"])) {
          $token = login_user($_POST["email"], $_POST["password"]);
          if($token) {
            setcookie('access_token', $token, time() + 86400, '/', $_SERVER["HTTP_HOST"], true, true );
            setcookie('logged_in', true, time() + 86400, "/", $_SERVER["HTTP_HOST"], false, false);
          }
          
        }
        header( "Location: {$_SERVER['REQUEST_URI']}", true, 303 );
        exit();
      }
      case 'register': {
        if (isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["username"])) {
          $userData = register_user($_POST["email"], $_POST["password"], $_POST["username"]);
        }
        header( "Location: {$_SERVER['REQUEST_URI']}", true, 303 );
        exit();
      }
  }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AppFuse: Software voor iedereen</title>
    <link rel="stylesheet" href="styles/style.css">
    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
  </head>