<?php
if (count($_SESSION['notifications']) > 0) {
    echo("<div class='notifications'>");
    foreach($_SESSION['notifications'] as $note) {
        echo ("
        <div class='note ".$note["state"]."'><span>".$note["message"]."</span></div>
        ");
    }
    echo("</div>");
}
unset($_SESSION['notifications']);
?>
<script defer src="scripts/notifier.js"></script>