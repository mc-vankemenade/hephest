<div class="header_bar">
    
    <?php if($_SERVER['REQUEST_URI'] != '/') {
        if(isset($listApps)) {
            $message = count($listApps) . " resultaten gevonden.";
        } else {
            $message = "";
        }

        echo (
            "<button onclick='history.back()' class='button back'>
                <img src='media/arrow_left.svg'>
            </button>
            <button onclick=window.location.assign('/') class='button home'>
                <img src='media/home.svg'>
            </button>
            <div class=results_bar>
                <span>" . $message . "<span>
            </div>"
        );
    } ?>
    <button class="button profile">
        <img src="media/user.svg">
    </button>
</div> 
<?php 
    include_once('login_prompt.php'); 
    include_once('notifier.php');
?>