<div class="login_prompt">
    <?php if(isset($_COOKIE["access_token"])) { ?>
        <div class="navigation">
        <button class='button back'>
            <img src='media/arrow_right.svg'>
        </button><span class="prompt_title">Je bent ingelogd. User acties zijn nog niet beschikbaar.</span></div>
   <?php } else { ?>
    <div class="navigation">
        <button class='button back'>
            <img src='media/arrow_right.svg'>
        </button><span class="prompt_title">Inloggen</span></div>
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <input type="hidden" name="action" value="login"/>
        <label for="username" style="display: none;">Gebruikersnaam</label>
        <input type="hidden" name="username"/>
        <label for="email">E-mailadres</label>
        <input type="text" name="email"/>
        <label for="password">Wachtwoord</label>
        <input type="password" name="password"/>
        <button type="submit" >Inloggen</button>
    </form>
    <div class="register"><span class="prompt_text">Nog geen account? </span><button class="register_button">Aanmelden</button></div>
    
    <?php } ?>
    <script defer src="/scripts/user_management.js"></script>
</div>