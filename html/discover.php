<?php
require_once('components/header.php');
?>
<body>
    <?php require_once('components/background_short.php'); ?>

    <div class="grid">
        <?php require_once('components/top_bar.php'); ?>

        <div class="centered">
            <div class="social_wrap">
                <div class="video_player">
                    <div class="media"></div>
                    <div class="next_media"></div>
                </div>
                <div class="controls">
                        <button class="bttn_previous"><img src="media/arrow_up.svg"></button>
                        <button class="bttn_next"><img src="media/arrow_down.svg"></button>
                        <button class="bttn_like"><img src="media/heart_line.svg"></button>
                        <button class="bttn_software"><img src="media/hammer.svg"></button>
                        <button class="bttn_comment"><img src="media/comment.svg"></button>
                </div>
                <div class="sidebar">
                    <div class="wrap_applications">
                    <div class=title_wrap><h3>Gemaakt met</h3><button class="close_section"><img src="/media/arrow_down.svg"/></button></div>
                        <ul class="app_list"></ul>
                        <a class="app_button" href="/results?app_id="><button class="app_button">Details ></button></a>
                    </div>
                    <div class="wrap_comments">
                        <div class=title_wrap><h3>Comments</h3><button class="close_section"><img src="/media/arrow_down.svg"/></button></div>
                        <ul class="comment_list"></ul>
                        <div class="new_comment">
                            <input class="comment_body" type="text">
                            <button type="submit" onclick="post_comment();"><img src='media/send.svg'></button>
                        <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js"></script>
    <script src="scripts/media_loader.js"></script>
</body>