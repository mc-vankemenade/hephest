<?php

function render_results($apps) {
    $html = '';

    if(is_array($apps)) { 
        foreach($apps as $app) {
            $html .= 
            "<li>
                <div class='img_wrap'><img src=" . $app->logo_url . "></div>
                <div class='text_wrap'><h2>" . $app->title . "</h2><p>" . $app->description ."</p><img id=" . $app->id . " class='select_button' src='media/select_line.svg'><img class='like_button' src='media/heart_line.svg'></div>
                <div class='price_wrap'>" . $app->specs->price . "</div>
                <a class='half_button' href='/discover?app_id=" . $app->id . "' ><button><i class='ri-video-line'></i> Meer creaties</button><a/><a href='/help?app_id=" . $app->id . "' class='half_button'><button><i class='ri-questionnaire-line'> </i>Hulpmiddelen</button></a>
                <a class='full_button'href=" . $app->download ."><button class='download_button'><i class='ri-download-2-line'></i> Download</button></a>
            </li>
            "; 
        }
    } else {
        $html = 
        "
            <li><span>Geen resultaten gevonden 🫤...</span></li>
        ";
    }
    return $html;
}

function render_help($helpers) {
    $html = '';
    if(is_array($helpers)) { 
        foreach($helpers as $app) {
            $html .= 
            "<li>
                <a href=". $app->content ." target='_blank' rel='noopener noreferrer'><img src=". $app->thumbnail ."></a>
                <div><a href=".  $app->content." target='_blank' rel='noopener noreferrer'><span>". $app->title  ."</span></a><img class='like_button' src='media/heart_line.svg'></div>
            </li>";
        }
    }
    return $html;
}