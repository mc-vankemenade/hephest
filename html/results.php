<?php
require_once('components/header.php');
require_once('lib/db_helper.php');
require_once('lib/render_helper.php');

if(isset($_GET['platform']) && isset($_GET['skill']) && isset($_GET['subject'])) {
    $listApps = get_apps(null, null, $_GET['platform'], $_GET['skill'], $_GET['subject']);
}

if(isset($_GET['app_id'])) {
    $listApps = get_apps(null, $_GET['app_id'], null, null, null);
}

?>
<body>
    <?php require_once('components/background_short.php'); ?>

    <div class="grid">
        <?php require_once('components/top_bar.php'); ?>


        <div class="centered">
            <?php require_once('components/app_compare.php'); ?>
            <ul class="list_wrap">
            <?php echo render_results($listApps); ?>
            </ul>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js"></script>
    <script src="scripts/app_compare.js"></script>
	<script src="scripts/results.js"></script>
</body>
