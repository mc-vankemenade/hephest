<?php 
require __DIR__ . '/../vendor/autoload.php';
require_once('lib/db_helper.php');
//require_once('lib/render_helper.php');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__."/..");
$dotenv->load();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    switch($_GET["action"]) {
        case 'get_posts': {
            if(isset($_GET["amount"]) && isset($_GET["offset"]) && isset($_GET["app_id"])) {
                $results = get_posts($_GET["amount"], $_GET["offset"], $_GET["app_id"]);
                echo json_encode($results);
                break;
            } 
            break;
        };
        case 'get_apps': {
            if( isset($_GET["app_id"])) {
                $results = get_apps(null, $_GET["app_id"]);
                echo json_encode($results);
                break;
            }
        }
        case 'get_comments': {
            if( isset($_GET["app_id"])) {
                $results = get_comments($_GET["app_id"]);
                echo json_encode($results);
                break;
            }
        }
        case 'post_comment': {
            if( isset($_GET["comment"]) && isset($_GET["post_id"]) && isset($_COOKIE["access_token"])) {
                $token = $_COOKIE["access_token"];
                $username = get_userdata($token)->user_metadata->username;
                post_comment($username, $_GET["comment"], $_GET["post_id"], $token);
                break;
            }
        }
        case 'get_metadata': {
            if(isset($_GET["id"])) {
                $results = get_metadata($_GET["id"]);
                echo json_encode($results);
                break;
            }
        };
        default: {
            http_response_code(418);
            break;
        }
    }
}