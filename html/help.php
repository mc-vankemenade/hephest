<?php
require_once('components/header.php');
require_once('lib/db_helper.php');
require_once('lib/render_helper.php');

if(isset($_GET['app_id'])) {
    $appInfo = get_apps(null, $_GET['app_id'], null, null, null);
    $migrationHelp = get_help($_GET['app_id'], 'migration');
    $beginnerHelp = get_help($_GET['app_id'], 'beginner');
    $advancedHelp = get_help($_GET['app_id'], 'advanced');
    $expertHelp = get_help($_GET['app_id'], 'expert');
} 

$dummy_help = (object) array(
    'title' => 'This Media has not been filled yet',
    'content' => '',
    'thumbnail' => 'https://zyyunltoixpjoyggpnku.supabase.co/storage/v1/object/public/application-media/OE60SH0.jpg',
);

if (count($beginnerHelp) < 10) {
    for($i = 0; $i < (12 - count($beginnerHelp)); $i++) {
        array_push($beginnerHelp, $dummy_help);
    }
}

if (count($advancedHelp) < 10) {
    for($i = 0; $i < (12 - count($advancedHelp)); $i++) {
        array_push($advancedHelp, $dummy_help);
    }
}

if (count($expertHelp) < 10) {
    for($i = 0; $i < (12 - count($expertHelp)); $i++) {
        array_push($expertHelp, $dummy_help);
    }
}

?>
<body>
    <?php require_once('components/background_short.php'); ?>

    <div class="grid">
        <?php require_once('components/top_bar.php'); ?>
    <?php if(isset($appInfo)) { ?>
        <div class="centered_column">
            <div class="migration_wrap">
                <h2>Overstappen naar <?php echo($appInfo[0]->title); ?>?</h2>
                <p>Hieronder staan een aantal handige links.</p>

            </div>
            <div class="help_wrap">
                <h2>Beginnen met <?php echo($appInfo[0]->title); ?></h2>
                <div class="control_wrap">
                    <button class="navigate previous"><</button>
                    <ul class="feed_wrap">
                        <?php echo render_help($beginnerHelp); ?>
                    </ul>
                    <button class="navigate next">></button>
                </div>
            </div>
            <div class="help_wrap">
                <h2><?php echo($appInfo[0]->title); ?> voor gevorderden</h2>
                <div class="control_wrap">
                    <button class="navigate previous"><</button>
                    <ul class="feed_wrap">
                        <?php echo render_help($advancedHelp); ?>
                    </ul>
                    <button class="navigate next">></button>
                </div>
            </div>
            <div class="help_wrap">
                <h2><?php echo($appInfo[0]->title); ?> Expert tips</h2>
                <div class="control_wrap">
                    <button class="navigate previous"><</button>
                    <ul class="feed_wrap">
                        <?php echo render_help($expertHelp); ?>
                    </ul>
                    <button class="navigate next">></button>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js"></script>
	<script src="scripts/help.js"></script>
</body>
