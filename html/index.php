<?php

$deviceTypes = array(
    "ios"    => "iphone",
    "ios "    => "ipad",
    "android" => "smartphone",
    "osx"       => "imac",
    "windows"    => "windows computer",
    "linux"    => "Linux computer"
);

$skillTypes = array(
    "beginner"    => "gemakkelijk",
    "intermediate"=> "hobbymatig",
    "professional"=> "professioneel"
);

$appTypes = array(
    "3dmodel"  => "3D modellen",
    "audio"     => "audio",
    "photo"  => "foto's",
    "game"     => "games",
    "interface"=> "interfaces",
    "music"     => "muziek",
    "painting" => "schilderijen",
    "drawing"   => "tekeningen",
    "video"    => "video's",
    "book"     => "boeken",
    "poster"   => "posters",
    
);

require_once('components/header.php');
?>
  <body>
    <?php require_once('components/background.php'); ?>

    <div class="grid">
        <?php require_once('components/top_bar.php'); ?>
        <div class="centered_column">
            <div class="logo"><img src="media/logo_white.svg"></div>
            <div class="search_bar">
                <form class="search_form" method="GET" action='/results'>
                <span>Ik wil</span>
                <?php // dynamisch ophalen van filters?? wellicht via een ajax.php bestand voor backend toegang?>
                <select name="skill" id="device_select">
                    <?php foreach ($skillTypes as $key => $type) {
                        echo ("<option value=" . $key . ">" . $type . "</option>");
                    }?>
                </select>
                <select name="subject" id="device_select">
                    <?php foreach ($appTypes as $key => $type) {
                        echo ("<option value=" . $key . ">" . $type . "</option>");
                    }?>
                </select>
                <span>maken op mijn</span>
                <select name="platform" id="device_select">
                    <?php foreach ($deviceTypes as $key => $type) {
                        echo ("<option value=" . $key . ">" . $type . "</option>");
                    }?>
                </select>
                </form>
                <button class="submit_button"><img src="media/search.svg"></button>
            </div>
                </div>
        <div class="footer_bar">
            <span>Weet je niet wat je zoekt?</span> 
            <button class="discover_button">Ontdekken ></button>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js"></script>
	<script src="scripts/scripts.js"></script>
  </body>
</html>
